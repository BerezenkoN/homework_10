package HomeWork_10;
import java.io.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by user on 31.10.2016.
 */
public class Person implements Serializable{

    String firstName;
    String lastName;
    String fullName;
    final String finalString;
    Sex sex;
    PrintStream outputStream;

    Person(String firstName, String lastName,  Sex sex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = String.format("%s, %s", lastName, firstName);
        this.finalString = "final string";
        this.sex = sex;
        this.outputStream = System.out;
    }

    @Override
    public String toString() {
        return firstName + " " +
                lastName + ", " +
                fullName + " " +
                sex + " " +
                finalString + "\n";
}
private  void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
}
    private void readObject(ObjectInputStream in) throws  IOException, ClassNotFoundException {
        in.defaultReadObject();
        String[] names = this.fullName.split(", ");
        this.lastName = names[0];
        this.firstName = names[1];
        this.outputStream = System.out;
    }
    }
enum Sex {
    MALE,
    FEMALE
}
class SerializePersons {
    public static void main(String[] args)throws IOException, ClassNotFoundException{
        Person solution = new Person("Alina", "Melnyk", Sex.FEMALE);
        System.out.println(solution);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(solution);

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
        Person solution_2 = (Person) ois.readObject();
        System.out.print(solution_2);

    }

}



