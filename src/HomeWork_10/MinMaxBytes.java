package HomeWork_10;


import java.io.*;

import java.util.Collections;
import java.util.HashMap;

import java.util.Map;
/**
 * Created by user on 31.10.2016.
 */
public class MinMaxBytes {
    public static void main(String[] args) throws Exception {
        Map<Byte, Integer> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());
        while (inputStream.available() > 0) ;
        {
            byte data = (byte)inputStream.read();
            if(map.containsKey(data)){
                map.put(data, map.get(data)+1);
            }
            else
            {
                map.put(data,1);
            }
        }
        int min = Collections.min(map.values());
        int max = Collections.max(map.values());
        for(Map.Entry <Byte,Integer> temp:map.entrySet())
        {
            if(temp.getValue().equals(max)) {
                System.out.print("Maximum" + temp.getKey());
            }
            else if(temp.getValue().equals(min))
            {
                System.out.print("Minimum" + temp.getKey());

            }
        }
        reader.close();
        inputStream.close();
    }
}
