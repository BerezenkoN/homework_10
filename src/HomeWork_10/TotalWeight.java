package HomeWork_10;

import java.io.*;

import java.util.Map;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by user on 31.10.2016.
 */
public class TotalWeight {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        Map<String, Integer> map = new HashMap<>();
        int max = 0;
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String name = scanner.next();
            int weight = scanner.nextInt();

            if (map.containsKey(name)) {
                map.put(name, map.get(name) + weight);
                max = (map.get(name) > max) ? map.get(name) : max;
            } else {
                map.put(name, weight);
                max = (map.get(name) > max) ? map.get(name) : max;
            }

        }
        scanner.close();

        for (Map.Entry<String, Integer> pair : map.entrySet())
            if (pair.getValue() == max)
                System.out.println(pair.getKey() + "" + pair.getValue() + "kg");
    }
}


