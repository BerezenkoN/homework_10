package HomeWork_10;

import java.io.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by user on 31.10.2016.
 */
public class TextTransformation  {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter source filename: \t");
        Scanner scanner = new Scanner(new File(reader.readLine()));
        System.out.print("Enter destination filename: \t");
        FileWriter fileWriter = new FileWriter(reader.readLine());
        boolean first = true;
        while(scanner.hasNext()){
            String[]line = scanner.nextLine().split("");
            String delimiter = first ? "" : ",";
            for(String s : line) {
                if(s.length() % 2 == 0) {
                    fileWriter.write(delimiter + s.substring(0, 1).toUpperCase() + s.substring(1));
                    first = false;
                }
            }
        }
        scanner.close();
        fileWriter.close();
    }
}
