package HomeWork_10;

import java.io.*;
import java.util.Scanner;

/**
 * Created by user on 31.10.2016.
 */
public class ReverseLines {
    public static void main(String[] args)throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        Scanner scanner = new Scanner(file);
        BufferedWriter fileWriter = new BufferedWriter( new FileWriter(reader.readLine()));
        while (scanner.hasNext()) {
            StringBuilder line = new StringBuilder(scanner.nextLine());
            fileWriter.write(line.reverse().toString());
            fileWriter.newLine();
        }
        scanner.close();
        fileWriter.close();
        }

    }

