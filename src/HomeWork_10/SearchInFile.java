package HomeWork_10;

import java.io.*;

import java.util.ArrayList;

import java.util.List;

import java.util.Scanner;

/**
 * Created by user on 31.10.2016.
 */
public class SearchInFile {
    public static void main(String[] args) throws Exception {
        List<String> searchedLines = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        File file = new File(reader.readLine());
        String wordFinder = reader.readLine();

        Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.contains(wordFinder))
                    searchedLines.add(line);
            }
        for(String s : searchedLines)
                System.out.println(s);
        reader.close();
        scanner.close();
        }


    }
