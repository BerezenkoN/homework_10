package HomeWork_10;

import java.io.*;
/**
 * Created by user on 31.10.2016.
 */
public class CopyFiles {
    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {

            System.out.print("Enter destination filename:\t");

            String destination = reader.readLine();

            System.out.print("Enter source filename:\t");

            String source = reader.readLine();

            FileInputStream inputStream = new FileInputStream(source);

            FileOutputStream outputStream = new FileOutputStream(destination);

            System.out.printf("Copying %s to %s...", source, destination);

            byte[] sourceContent = new byte[inputStream.available()];

            inputStream.read(sourceContent);

            outputStream.write(sourceContent);

            inputStream.close();

            outputStream.close();

            reader.close();

            System.out.println("Complete");

        } catch (FileNotFoundException e) {

            System.out.println("Can't find file");

            e.printStackTrace();

        } catch (IOException e) {

            System.out.println("An I/O exception");

            e.printStackTrace();

        }

    }


}

